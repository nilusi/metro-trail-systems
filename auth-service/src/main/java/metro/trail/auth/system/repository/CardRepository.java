package metro.trail.auth.system.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import metro.trail.auth.system.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, String> {

	Card findByCardId(String cardId);
}
