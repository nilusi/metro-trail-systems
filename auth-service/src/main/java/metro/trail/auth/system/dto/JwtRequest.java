package metro.trail.auth.system.dto;

import java.io.Serializable;

public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	
	private String cardId;
	private String secretPin;
	
	//need default constructor for JSON Parsing
	public JwtRequest()
	{
		
	}

	public JwtRequest(String cardId, String secretPin) {
		this.setCardId(cardId);
		this.setSecretPin(secretPin);
	}

	public String getCardId() {
		return this.cardId;
	}

	public void setCardId(String username) {
		this.cardId = username;
	}

	public String getSecretPin() {
		return this.secretPin;
	}

	public void setSecretPin(String password) {
		this.secretPin = password;
	}
}