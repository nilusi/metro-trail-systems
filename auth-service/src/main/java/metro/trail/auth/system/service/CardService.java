package metro.trail.auth.system.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import metro.trail.auth.system.dto.JwtRequest;
import metro.trail.auth.system.model.Card;
import metro.trail.auth.system.repository.CardRepository;

@Component
public class CardService implements UserDetailsService {

	 	@Autowired
	    private CardRepository cardRepository;

	    @Autowired
	    private PasswordEncoder bcryptEncoder;

	    public UserDetails loadUserByUsername(String cardId) throws UsernameNotFoundException {
	        Card user = cardRepository.findByCardId(cardId);
	        if (user == null) {
	            throw new UsernameNotFoundException("Card not found with cardId: " + cardId);
	        }
	        return new org.springframework.security.core.userdetails.User(user.getCardId(), user.getSecretPin(),
	                new ArrayList<>());
	    }

	    public Card save(JwtRequest authenticationRequest) {
	        Card newUser = new Card();
	        newUser.setCardId(authenticationRequest.getCardId());
	        newUser.setSecretPin(bcryptEncoder.encode(authenticationRequest.getSecretPin()));
	        return cardRepository.save(newUser);
	    }
}
