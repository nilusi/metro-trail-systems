package metro.trail.auth.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "Card")
@Entity
public class Card {

	@Id
	private String cardId; 
	private String secretPin; //secret pin

	
	public String getCardId() {
		return cardId;
	}

	public void setCardId(String userId) {
		this.cardId = userId;
	}

	public String getSecretPin() {
		return secretPin;
	}

	public void setSecretPin(String secretPin) {
		this.secretPin = secretPin;
	}

}
