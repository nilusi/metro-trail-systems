package com.metro.passenger.management.system.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.metro.passenger.management.system.model.Passenger;

@Repository
public interface PassengerRepository extends CrudRepository<Passenger,Long>{

}
