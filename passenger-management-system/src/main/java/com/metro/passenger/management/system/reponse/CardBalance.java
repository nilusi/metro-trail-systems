package com.metro.passenger.management.system.reponse;


public class CardBalance extends CommonResponse{

	private double balance;
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
}
