package com.metro.passenger.management.system.request;

import org.springframework.stereotype.Component;

@Component
public class SmartCardApplyRequest {

	
	private Long userId;
	private String userName;
	private String address;
	private String secretPin;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSecretPin() {
		return secretPin;
	}

	public void setSecretPin(String secretPin) {
		this.secretPin = secretPin;
	}

}
