package com.metro.passenger.management.system.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.metro.passenger.management.system.model.Payment;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {

	List<Payment> findByCardId(Long cardId);

	List<Payment> findByCardIdOrderByCreatedDateDesc(Long cardId);

	List<Payment> findAllByCardIdOrderByCreatedDateDesc(Long cardId);
	List<Payment> findTop10ByCardIdOrderByCreatedDateDesc(Long cardId);

	Optional<Payment> findTop1ByCardIdAndPaidDateNotNullOrderByPaidDateDesc(Long cardId);

}
