package com.metro.passenger.management.system.util;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class CommonConverter {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	MessageSource messageSource;

	@SuppressWarnings("unchecked")
	public Map<String, Object> pojoToMap(Object object) {
		return objectMapper.convertValue(object, Map.class);
	}

	public <T> T mapToPojo(Map<String, Object> map, Class<T> toValueType) {
		return objectMapper.convertValue(map, toValueType);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> jsonToMap(String json) {
		try {
			return objectMapper.readValue(json, Map.class);
		} catch (IOException e) {
			//handle here
		}
		return null;
	}

	public <T> T jsonToObject(String json, Class<T> toValueType) {
		try {
			return objectMapper.readValue(json, toValueType);
		} catch (IOException e) {
			//should handle
		}
		return null;
	}
}
