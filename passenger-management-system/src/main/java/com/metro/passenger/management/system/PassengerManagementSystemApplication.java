package com.metro.passenger.management.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
public class PassengerManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassengerManagementSystemApplication.class, args);
	}

}
