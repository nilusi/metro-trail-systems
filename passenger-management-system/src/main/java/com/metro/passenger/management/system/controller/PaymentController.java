package com.metro.passenger.management.system.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metro.passenger.management.system.dto.Trip;
import com.metro.passenger.management.system.model.Payment;
import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.service.PaymentService;


@RestController
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@PostMapping(value = "/topup")
	public ResponseEntity<CommonResponse> topUp(Long cardId, double amount) {

		CommonResponse response = paymentService.topUp(cardId, amount);
		return ResponseEntity.status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
				.body(response);
	}
	
	@PostMapping(value = "/add")
	public void addTransaction(Trip trip){
		paymentService.addTransaction(trip);
	}
	
	
	
	@PostMapping(value = "/transaction")
	public ResponseEntity<List<Payment>> getLastTransactions(Long cardId){
		List<Payment> response = paymentService.getLastTransactions(cardId);

		return ResponseEntity.ok(response);
	}
}
