package com.metro.passenger.management.system.util;

public enum SmartCardStatus {
	ACTIVE, INACTIVE;
}
