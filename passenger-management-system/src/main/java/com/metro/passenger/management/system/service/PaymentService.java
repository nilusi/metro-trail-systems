package com.metro.passenger.management.system.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.metro.passenger.management.system.dto.Trip;
import com.metro.passenger.management.system.model.Payment;
import com.metro.passenger.management.system.model.SmartCard;
import com.metro.passenger.management.system.reponse.CardBalance;
import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.repository.PaymentRepository;
import com.metro.passenger.management.system.repository.SmartCardRepository;
import com.metro.passenger.management.system.util.SmartCardStatus;


@Component
public class PaymentService {

	@Autowired
	private PaymentRepository paymentRepository;
	@Autowired
	private SmartCardRepository smartCardRepository;
	@Autowired
	private SmartCardService smartCardService;

	public CommonResponse topUp(Long cardId, double amount) {
		CommonResponse response  = new CommonResponse();
		
		Optional<SmartCard> smartCardOpt = smartCardRepository.findByCardIdAndStatus(cardId, SmartCardStatus.ACTIVE);

		
		
		if(!smartCardOpt.isPresent()){
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setReturnMessage("Card is invalid");
			return response;
		}
		
		SmartCard smartCard = smartCardOpt.get();
		Payment payment = new Payment();
		payment.setCardId(cardId);
		payment.setAmount(amount);
		payment.setTopUpDate(new Date());
		
		paymentRepository.save(payment);
		Optional<Payment> paymentOpt = paymentRepository.findTop1ByCardIdAndPaidDateNotNullOrderByPaidDateDesc(cardId);

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -7);
		Date dateBefore7Days = cal.getTime();  
		//change to chain of responsibility pattern
		if(smartCard.getBalance()>=0 || ( paymentOpt.isPresent() && paymentOpt.get().getPaidDate().after(dateBefore7Days))){
			smartCard.setBalance(smartCard.getBalance() + amount);
		}
		//balance < 0 and Date greater than 7
		if(smartCard.getBalance()<0 &&  paymentOpt.isPresent() && !paymentOpt.get().getPaidDate().after(dateBefore7Days)){
			double penalty = paymentOpt.get().getFareAmount() * 0.3;
			smartCard.setBalance(smartCard.getBalance() + amount - penalty);
		}
		smartCardRepository.save(smartCard);
		response.setReturnMessage("Successfully topup your card with Rs: " + amount);
		response.setStatusCode(HttpStatus.OK.value());
		return response;
	}

	public void addTransaction(Trip trip) {

		Payment payment = new Payment();
		//payment.setAmount(trip.getFareAmount());
		payment.setCardId(trip.getCardId());
		payment.setPaidDate(new Date());
		payment.setFareAmount(trip.getFareAmount());
		paymentRepository.save(payment);
		
		smartCardService.deductBalance(trip.getCardId(), trip.getFareAmount());
	}

	

	public List<Payment> getLastTransactions(Long cardId) {

		List<Payment> transactionList = paymentRepository.findTop10ByCardIdOrderByCreatedDateDesc(cardId);
		
		return transactionList;
	}
	

}
