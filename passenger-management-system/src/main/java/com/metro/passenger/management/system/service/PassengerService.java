package com.metro.passenger.management.system.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.metro.passenger.management.system.exception.InvalidRequestException;
import com.metro.passenger.management.system.model.Passenger;
import com.metro.passenger.management.system.model.SmartCard;
import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.repository.PassengerRepository;
import com.metro.passenger.management.system.repository.SmartCardRepository;
import com.metro.passenger.management.system.request.SmartCardApplyRequest;
import com.metro.passenger.management.system.util.SmartCardStatus;

@Component
public class PassengerService {

	
	
	@Autowired
	private PassengerRepository passengerRepository;
	@Autowired
	private SmartCardRepository smartCardRepository;
	@Autowired
	private SmartCardService smartCardService;
	
	@Value("${min.Amt.travel}")
	private double minAmount;

	public CommonResponse applyForSmartCard(SmartCardApplyRequest request) {

		CommonResponse response = new CommonResponse();
		Passenger passenger = new Passenger();
		passenger.setAddress(request.getAddress());
		passenger.setSecretPin(request.getSecretPin());
		passenger.setUserName(request.getUserName());

		passengerRepository.save(passenger);

		response = smartCardService.createSmartCard(passenger);
		
		return response;
	}
	
	public CommonResponse checkIn(Long cardId){
		CommonResponse response = new CommonResponse();
		
		Optional<SmartCard> cardBalanceOpt = smartCardRepository.findById(cardId);
		
		if(!cardBalanceOpt.isPresent()){
			throw new InvalidRequestException("Card is not available");
		}
		
		SmartCard cardBalance = cardBalanceOpt.get();
		
		if(SmartCardStatus.INACTIVE.equals(cardBalance.getStatus())){
			throw new InvalidRequestException("Smart card is no longer active. Plz apply for new card");

		}
		if(minAmount>=cardBalance.getBalance()){
			throw new InvalidRequestException("You don't have enough balance to travel.. Plz top up");
		}
		//Have to add the trip start
		response.setReturnMessage("Happy Travel");
		response.setStatusCode(HttpStatus.OK.value());
		return response;
	}

}
