package com.metro.passenger.management.system.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.metro.passenger.management.system.util.SmartCardStatus;

@Entity
@Table
public class SmartCard {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long cardId;
	private SmartCardStatus status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	@Temporal(TemporalType.TIMESTAMP)
	private Date deactivatedOn;
	private String reason;
	
	private Long userId;
	private double balance;

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long passengerId) {
		this.userId = passengerId;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Long getCardId() {
		return cardId;
	}
	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}
	public SmartCardStatus getStatus() {
		return status;
	}
	public void setStatus(SmartCardStatus status) {
		this.status = status;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
