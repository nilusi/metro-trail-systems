package com.metro.rail.system.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.metro.rail.system.model.Trip;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

	List<Trip> findByCardId(Long cardId);

	List<Trip> findByCardIdOrderByEndDateDesc(Long cardId);

}
