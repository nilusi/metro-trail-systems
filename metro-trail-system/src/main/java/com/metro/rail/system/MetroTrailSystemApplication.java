package com.metro.rail.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetroTrailSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetroTrailSystemApplication.class, args);
	}

}
