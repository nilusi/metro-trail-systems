package com.metro.rail.system.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metro.rail.system.dto.CardBalance;
import com.metro.rail.system.exception.InvalidRequestException;
import com.metro.rail.system.response.CommonResponse;
import com.metro.rail.system.service.TripService;

@RestController
@RequestMapping("/trip")
public class TripController {

	@Autowired
	TripService tripService;

	@PostMapping(value = "/checkout")
	public ResponseEntity<CommonResponse> checkOut(Long cardId, String origin, String destination) {

		CommonResponse response;
		try {
			response = tripService.checkout(cardId, origin, destination);
		} catch (InvalidRequestException e) {
			response = new CommonResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage());
		}
		return ResponseEntity.status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
				.body(response);
	}


}
