package com.metro.rail.system.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.metro.rail.system.model.Fare;

@Repository
public interface FareRepository extends CrudRepository<Fare, Long>{

	Fare findByOriginAndDestination(String origin, String destination);

}
